var axios = require("axios");

module.exports = async (req, res) => {
    var response = await axios.get("https://api.covid19api.com/summary");
    //   console.log(response.data.Countries)
    var specificCountry = response.data.Countries;
    var wantedData = specificCountry.filter(function (i) {
      return i.Country === req.body.Country;
    });
    var object = (({
      Country,
      NewConfirmed,
      TotalConfirmed,
      NewDeaths,
      TotalDeaths,
      NewRecovered,
      TotalRecovered,
    }) => ({
      Country,
      NewConfirmed,
      TotalConfirmed,
      NewDeaths,
      TotalDeaths,
      NewRecovered,
      TotalRecovered,
    }))(wantedData[0]);
  
    res.send(object);
  }