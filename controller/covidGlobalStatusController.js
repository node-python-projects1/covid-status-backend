var axios = require("axios")

module.exports = (req, res) => {
    axios
      .get("https://api.covid19api.com/summary")
      .then((response) => {
        res.send(response.data.Global);
        // console.log(response.data.Global)
      })
      .catch((err) => {
        res.send(err);
      });
  }