var presentDate = require("./currentDate");
var axios = require("axios");

module.exports = (req, res) => {
  axios
    .get("https://api.covid19api.com/live/country/India")
    .then((response) => {
        console.log(response.data.Countries)
      var specificState = response.data;
      var wantedData = specificState.filter(function (i) {
        return i.Province === req.body.Province && i.Date === presentDate;
      });
      var object = (({ Province, Confirmed, Deaths, Recovered, Active }) => ({
        Province,
        Confirmed,
        Deaths,
        Recovered,
        Active,
      }))(wantedData[0]);
      res.send(object);
    })
    .catch((err) => {
      res.send(err);
    });
};
