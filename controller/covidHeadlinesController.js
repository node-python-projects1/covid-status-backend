var axios = require("axios");
var presentDate = require("./currentDate");
let top_countries_count = [];
let top_states_count = [];
let countries_data = [];

module.exports = async (req, res) => {
  try {
    var country_response = await axios.get(
      "https://api.covid19api.com/summary"
    );
    var response = await axios.get(
      "https://api.covid19api.com/live/country/India"
    );
    var specificState = response.data;
    var wantedData = specificState.filter(function (i) {
      return i.Date === presentDate;
    });

    for (i = 0; i < country_response.data.Countries.length; i++) {
      var object = (({ Country, NewConfirmed }) => ({
        Country,
        NewConfirmed,
      }))(country_response.data.Countries[i]);
      top_countries_count.push(object);
    }

    for (i = 0; i < wantedData.length; i++) {
      var object = (({ Province, Confirmed, Deaths, Recovered, Active }) => ({
        Province,
        Confirmed,
        Deaths,
        Recovered,
        Active,
      }))(wantedData[i]);
      countries_data.push(object);
    }

    for (i = 0; i < countries_data.length; i++) {
      var object = (({ Province, Active }) => ({
        Province,
        Active,
      }))(countries_data[i]);
      top_states_count.push(object);
    }
    const result = {
      Country: "",
      NewConfirmed: -Infinity,
      Province: "",
      Active: -Infinity,
    };
    await top_countries_count.forEach((el) => {
      const Country = el["Country"];
      const NewConfirmed = el["NewConfirmed"];
      if (NewConfirmed > result.NewConfirmed) {
        result.Country = Country;
        result.NewConfirmed = NewConfirmed;
      }
    });
    await top_states_count.forEach((el) => {
      const Province = el["Province"];
      const Active = el["Active"];
      if (Active > result.Active) {
        result.Province = Province;
        result.Active = Active;
      }
    });

    return res.json(result);
  } catch (err) {
    console.log(err);
    res.send(err);
  }
};
