function pad(n) {
  return n < 10 ? "0" + n : n;
}

const currentDate = new Date();

const currentDayOfMonth = currentDate.getDate();
const currentMonth = currentDate.getMonth(); // Be careful! January is 0, not 1
const currentYear = currentDate.getFullYear();

const dateString =
  currentYear + "-" + pad(currentMonth + 1) + "-" + currentDayOfMonth;

const presentDate = dateString + "T00:00:00Z";
// console.log(presentDate);
module.exports = presentDate;
