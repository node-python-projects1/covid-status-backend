var axios = require("axios");

let countries_data = [];

module.exports = async (req, res) => {
  var response = await axios.get("https://api.covid19api.com/summary");
  // console.log(response.data);
  // res.send(response.data.Countries);
  for (i = 0; i < response.data.Countries.length; i++) {
    var object = (({
      Country,
      NewConfirmed,
      TotalConfirmed,
      NewDeaths,
      TotalDeaths,
      NewRecovered,
      TotalRecovered,
    }) => ({
      Country,
      NewConfirmed,
      TotalConfirmed,
      NewDeaths,
      TotalDeaths,
      NewRecovered,
      TotalRecovered,
    }))(response.data.Countries[i]);
    countries_data.push(object);
  }
  res.send(countries_data);
};
