var presentDate = require("./currentDate");

var axios = require("axios");

module.exports = async (req, res) => {
  var response = await axios.get(
    "https://api.covid19api.com/live/country/India"
  );
  let countries_data = [];
  var specificState = response.data;
  var wantedData = specificState.filter(function (i) {
    return i.Date === presentDate;
  });
  for (i = 0; i < wantedData.length; i++) {
    var object = (({ Province, Confirmed, Deaths, Recovered, Active }) => ({
      Province,
      Confirmed,
      Deaths,
      Recovered,
      Active,
    }))(wantedData[i]);
    countries_data.push(object);
  }
  res.send(countries_data);
};
