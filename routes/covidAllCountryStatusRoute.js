var express = require("express");


const covidAllCountryStatusController = require("../controller/covidAllCountryStatusController");
var router = express.Router();
var app = express();

app.use(express.json());

// http://localhost:9000/covidAllCountryStatus
router.get("/covidAllCountryStatus",covidAllCountryStatusController);

module.exports = router;
