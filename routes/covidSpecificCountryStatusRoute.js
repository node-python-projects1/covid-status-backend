var express = require("express");
const covidSpecificCountryStatusController = require("../controller/covidSpecificCountryStatusController");
var router = express.Router();
var app = express();

app.use(express.json());

// http://localhost:9000/covidSpecificCountryStatus
router.post(
  "/covidSpecificCountryStatus",
  covidSpecificCountryStatusController
);

module.exports = router;
