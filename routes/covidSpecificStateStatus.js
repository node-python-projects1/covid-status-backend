var express = require("express");

const covidSpecificStateStatusContoller = require("../controller/covidSpecificStateStatusContoller");
var router = express.Router();
var app = express();

app.use(express.json());

// http://localhost:9000/covidSpecificStateStatus
router.post("/covidSpecificStateStatus", covidSpecificStateStatusContoller );

module.exports = router;
