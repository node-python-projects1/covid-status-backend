var express = require("express");


const covidAllStateStatusController = require("../controller/covidAllStateStatusController");
var router = express.Router();
var app = express();

app.use(express.json());


// http://localhost:9000/covidAllStateStatus
router.get("/covidAllStateStatus", covidAllStateStatusController);

module.exports = router;
