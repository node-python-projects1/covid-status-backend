var express = require("express");

const covidGlobalStatusController = require("../controller/covidGlobalStatusController");
var router = express.Router();
var app = express();

app.use(express.json());

// http://localhost:9000/covidGlobalStatus
router.get("/covidGlobalStatus", covidGlobalStatusController);

module.exports = router;
