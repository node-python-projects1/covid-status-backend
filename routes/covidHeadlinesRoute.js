var express = require("express");

const covidHeadlinesController = require("../controller/covidHeadlinesController.js");
var router = express.Router();
var app = express();

app.use(express.json());

// http://localhost:9000/covidHeadlines
router.get("/covidHeadlines", covidHeadlinesController);

module.exports = router;
