var users = {
  name: "John",
  friends: [
    {
      id: 1,
      name: "George",
      level: 10,
    },
    {
      id: 2,
      name: "Stacy",
      level: 8,
    },
    {
      id: 3,
      name: "Fred",
      level: 10,
    },
    {
      id: 4,
      name: "Amy",
      level: 7,
    },
    {
      id: 5,
      name: "Bob",
      level: 10,
    },
  ],
};
var wantedData = users.friends.filter(function (i) {
  return i.name === "George";
});
console.log(wantedData);
