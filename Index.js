var express = require("express");
var cors = require("cors");

var covidGlobalRoute = require("./routes/covidGlobalStatusRoute");
var covidAllCountriesRoute = require("./routes/covidAllCountryStatusRoute");
var covidSpecificCountryRoute = require("./routes/covidSpecificCountryStatusRoute");
var covidAllStateStatusRoute = require("./routes/covidAllStateStatus");
var covidSpecificStateRoute = require("./routes/covidSpecificStateStatus");
var covidHeadlinesRoute = require("./routes/covidHeadlinesRoute");

var app = express();

app.use(cors());

app.use(express.json());

app.use(covidGlobalRoute);

app.use(covidAllCountriesRoute);

app.use(covidSpecificCountryRoute);

app.use(covidAllStateStatusRoute);

app.use(covidSpecificStateRoute);

app.use(covidHeadlinesRoute);

app.listen("9000", (err) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Server is up and running at port 9000");
  }
});
