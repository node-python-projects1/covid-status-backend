COVID BACKEND APPLICATION -

This application provides -

* Global Covid cases status such as 

    "NewConfirmed",
    "TotalConfirmed"
    "NewDeaths"
    "TotalDeaths"
    "NewRecovered"
    "TotalRecovered"
    "Date"

[http://localhost:9000/covidGlobalStatus]

* All country covid cases status such as  

[
    {
        "Country 1"
        "NewConfirmed"
        "TotalConfirmed"
        "NewDeaths"
        "TotalDeaths"
        "NewRecovered"
        "TotalRecovered"
    },
    .
    .
    .
    .
    {
        "Country n"
        "NewConfirmed"
        "TotalConfirmed"
        "NewDeaths"
        "TotalDeaths"
        "NewRecovered"
        "TotalRecovered"
    }

]

[http://localhost:9000/covidAllCountryStatus]
    
* Specific Country Status such as

        "Country"
        "NewConfirmed"
        "TotalConfirmed"
        "NewDeaths"
        "TotalDeaths"
        "NewRecovered"
        "TotalRecovered"

[http://localhost:9000/covidSpecificCountryStatus]

* All states covid cases status such as

[
    {
        "Province/State Name 1"
        "Confirmed"
        "Deaths"
        "Recovered"
        "Active"
    },
    .
    .
    .
    {
        "Province/State Name 'n'"
        "Confirmed"
        "Deaths"
        "Recovered"
        "Active"
    }
]

[http://localhost:9000/covidAllStateStatus]

* Specific State covid cases status such as

        "Province/State Name"
        "Confirmed"
        "Deaths"
        "Recovered"
        "Active"

[http://localhost:9000/covidSpecificStateStatus]